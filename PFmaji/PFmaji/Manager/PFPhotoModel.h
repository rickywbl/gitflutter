//
//  PFPhotoModel.h
//  PFmaji
//
//  Created by perfayMini on 2018/12/6.
//  Copyright © 2018 huawei. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum : NSUInteger {
    PFPhotoModelMediaTypePhoto          = 0,    //!< 照片
    PFPhotoModelMediaTypeLivePhoto      = 1,    //!< LivePhoto
    PFPhotoModelMediaTypePhotoGif       = 2,    //!< gif图
    PFPhotoModelMediaTypeVideo          = 3,    //!< 视频
    PFPhotoModelMediaTypeAudio          = 4,    //!< 预留
    PFPhotoModelMediaTypeCameraPhoto    = 5,    //!< 通过相机拍的照片
    PFPhotoModelMediaTypeCameraVideo    = 6,    //!< 通过相机录制的视频
    PFPhotoModelMediaTypeCamera         = 7     //!< 跳转相机
} PFPhotoModelMediaType;

typedef enum : NSUInteger {
    PFPhotoModelMediaSubTypePhoto = 0,  //!< 照片
    PFPhotoModelMediaSubTypeVideo       //!< 视频
} PFPhotoModelMediaSubType;

NS_ASSUME_NONNULL_BEGIN

@interface PFPhotoModel : NSObject

/**
 创建日期
 
 - 如果是通过相机拍摄的并且没有保存到相册(临时的) 为当前时间([NSDate date])
 */
@property (strong, nonatomic) NSDate *creationDate;
/**
 修改日期
 
 - 如果是通过相机拍摄的并且没有保存到相册(临时的) 为当前时间([NSDate date])
 */
@property (strong, nonatomic) NSDate *modificationDate;
/**
 位置信息 NSData 对象
 
 - 如果是通过相机拍摄的并且没有保存到相册(临时的) 没有值
 */
@property (strong, nonatomic) NSData *locationData;

/**
 位置信息 CLLocation 对象
 
 - 通过相机拍摄的时候有定位权限的话就有值
 */
@property (strong, nonatomic) CLLocation *location;

/**  临时的列表小图  */
@property (strong, nonatomic) UIImage *thumbPhoto;

/**  临时的预览大图  */
@property (strong, nonatomic) UIImage *previewPhoto;

/**  当前照片所在相册的名称 */
@property (copy, nonatomic) NSString *albumName;


/**  是否iCloud上的资源  */
@property (nonatomic, assign) BOOL isICloud;

/**  照片PHAsset对象  */
@property (strong, nonatomic) PHAsset *asset;

@property (assign, nonatomic) NSInteger currentAlbumIndex;


/**
 小图照片清晰度 越大越清晰、越消耗性能。太大可能会引起界面卡顿
 默认设置：[UIScreen mainScreen].bounds.size.width
 320    ->  0.8
 375    ->  1.4
 other  ->  1.7
 */
@property (assign, nonatomic) CGFloat clarityScale;

@property (assign, nonatomic) PFPhotoModelMediaType type;
/**  照片子类型  */
@property (assign, nonatomic) PFPhotoModelMediaSubType subType;


/**  模型对应的Section */
@property (assign, nonatomic) NSInteger dateSection;
/**  模型对应的item */
@property (assign, nonatomic) NSInteger dateItem;


@end

NS_ASSUME_NONNULL_END
