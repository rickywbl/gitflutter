//
//  PFPhotoManager.h
//  PFmaji
//
//  Created by perfayMini on 2018/12/5.
//  Copyright © 2018 huawei. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PFPhotoModel;


NS_ASSUME_NONNULL_BEGIN

/**
 *  照片选择器的管理类, 使用照片选择器时必须先懒加载此类,然后赋值给对应的对象
 */
typedef NS_ENUM(NSUInteger, PFPhotoManagerSelectedType) {
    PFPhotoManagerSelectedTypePhoto = 0,        //!< 只显示图片
    PFPhotoManagerSelectedTypeVideo = 1,        //!< 只显示视频
    PFPhotoManagerSelectedTypePhotoAndVideo     //!< 图片和视频一起显示
};


@interface PFPhotoManager : NSObject

@property(nonatomic,strong)NSMutableArray * allAlbums;
@property (strong, nonatomic) NSMutableArray *iCloudUploadArray;
@property (strong, nonatomic) NSMutableArray *selectedList;



@property(nonatomic,assign)PFPhotoManagerSelectedType type;

- (void)getPhotoListWithAlbumModel:(PFAlbumModel *)albumModel complete:(void (^)(NSArray *allList , NSArray *previewList,NSArray *photoList ,NSArray *videoList ,NSArray *dateList , PFPhotoModel *firstSelectModel))complete;


@end

NS_ASSUME_NONNULL_END
