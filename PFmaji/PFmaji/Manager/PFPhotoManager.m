//
//  PFPhotoManager.m
//  PFmaji
//
//  Created by perfayMini on 2018/12/5.
//  Copyright © 2018 huawei. All rights reserved.
//

#import "PFPhotoManager.h"
#import "PFPhotoConfiguration.h"


NS_ASSUME_NONNULL_BEGIN
@interface PFPhotoManager ()
@property(nonatomic,retain)PHCachingImageManager * imageManager;
@property(nonatomic,retain)PHImageRequestOptions * imageRequestOptions;
@property(nonatomic,retain)PFPhotoConfiguration * configuration;

@end
NS_ASSUME_NONNULL_END

@implementation PFPhotoManager

//static PFPhotoManager * _manager;

//+ (instancetype)shareInstance{
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        _manager = [[PFPhotoManager alloc] init];
//    });
//    return _manager;
//}

- (instancetype)initWithType:(PFPhotoManagerSelectedType)type {
    if (self = [super init]) {
        self.type = type;
        [self setup];
    }
    return self;
}
- (instancetype)init {
    if ([super init]) {
        self.type = PFPhotoManagerSelectedTypePhoto;
        [self setup];
    }
    return self;
}



- (void)setup{
    self.allAlbums = [[NSMutableArray alloc] init];
    self.iCloudUploadArray = [[NSMutableArray alloc] init];
    self.selectedList = [[NSMutableArray alloc] init];
    [self fetchAlbums];
}


#pragma mark ------ 获取相册


- (void)fetchAlbums{
    PHFetchResult *allAlbums =  [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    __block NSMutableArray * albums = [[NSMutableArray alloc] init];
    [allAlbums enumerateObjectsWithOptions:(NSEnumerationConcurrent) usingBlock:^(PHAssetCollection * collection, NSUInteger idx, BOOL * _Nonnull stop) {
        // 是否按创建时间排序
        PHFetchOptions *option = [[PHFetchOptions alloc] init];
        if (self.configuration.creationDateSort) {
            option.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
        }
        if(self.type == PFPhotoManagerSelectedTypePhoto){
            option.predicate = [NSPredicate predicateWithFormat:@"mediaType == %ld", PHAssetMediaTypeImage];
        } if (self.type == PFPhotoManagerSelectedTypeVideo) {
            option.predicate = [NSPredicate predicateWithFormat:@"mediaType == %ld", PHAssetMediaTypeVideo];
        }
        
        @autoreleasepool {
            PHFetchResult * result =  [PHAsset fetchAssetsInAssetCollection:collection options:option];
            if(result.count > 0){
                PFAlbumModel * model = [[PFAlbumModel alloc] init];
                model.count = result.count;
                model.albumName = collection.localizedTitle;
                model.result = result;
                model.asset = [result lastObject];
                [albums addObject:model];
            }
        }
        
    }];
    
    self.allAlbums = [albums mutableCopy];
}


- (void)getPhotoListWithAlbumModel:(PFAlbumModel *)albumModel complete:(void (^)(NSArray * _Nonnull, NSArray * _Nonnull, NSArray * _Nonnull, NSArray * _Nonnull, NSArray * _Nonnull, PFPhotoTools * _Nonnull))complete{
    NSMutableArray *allArray = [NSMutableArray array];
    NSMutableArray *previewArray = [NSMutableArray array];
    NSMutableArray *videoArray = [NSMutableArray array];
    NSMutableArray *photoArray = [NSMutableArray array];
    NSMutableArray *dateArray = [NSMutableArray array];
    
    
    __block NSDate *currentIndexDate;
    __block NSMutableArray *sameDayArray;
    
    __block PFPhotoDateModel *dateModel;
    __block PFPhotoModel *firstSelectModel;
    __block BOOL already = NO;
    NSMutableArray *selectList = [NSMutableArray arrayWithArray:self.selectedList];

    if(self.configuration.reverseDate){
        [albumModel.result enumerateObjectsUsingBlock:^(PHAsset * asset, NSUInteger idx, BOOL * _Nonnull stop) {
            
            PFPhotoModel * photoModel = [[PFPhotoModel alloc]  init];
            photoModel.clarityScale = self.configuration.clarityScale;
            photoModel.asset = asset;
            
            @autoreleasepool {
                if([[asset valueForKey:@"isCloudPlaceholder"] boolValue]){
                    if(self.iCloudUploadArray.count){
                        NSPredicate *pred = [NSPredicate predicateWithFormat:@"localIdentifier = %@", asset.localIdentifier];
                        NSArray *newArray = [self.iCloudUploadArray filteredArrayUsingPredicate:pred];
                        if (!newArray.count) {
                            photoModel.isICloud = YES;
                        }
                    }else{
                        photoModel.isICloud = YES;
                    }
                }
            }
            
        }];
    }else{
        NSInteger index = 0;
        for (PHAsset * asset in albumModel.result) {
            PFPhotoModel * photoModel = [[PFPhotoModel alloc]  init];
            photoModel.clarityScale = self.configuration.clarityScale;
            photoModel.asset = asset;
            
            @autoreleasepool {
                if([[asset valueForKey:@"isCloudPlaceholder"] boolValue]){
                    if(self.iCloudUploadArray.count){
                        NSPredicate *pred = [NSPredicate predicateWithFormat:@"localIdentifier = %@", asset.localIdentifier];
                        NSArray *newArray = [self.iCloudUploadArray filteredArrayUsingPredicate:pred];
                        if (!newArray.count) {
                            photoModel.isICloud = YES;
                        }
                    }else{
                        photoModel.isICloud = YES;
                    }
                }
                if(selectList.count > 0){
                    
                }
                if(asset.mediaType == PHAssetMediaTypeImage){
                    photoModel.subType = PFPhotoModelMediaSubTypePhoto;
                    if([[asset valueForKey:@"filename"] hasSuffix:@"GIF"]){
                        if(self.configuration.singleSelected){
                            photoModel.type = PFPhotoModelMediaTypePhoto;
                        }else{
                            photoModel.type = PFPhotoModelMediaTypePhotoGif;
                        }
                    }else if(asset.mediaSubtypes == PHAssetMediaSubtypePhotoLive){
                        if(iOS9Later){
                            if (!self.configuration.singleSelected) {
                                photoModel.type = PFPhotoModelMediaTypeLivePhoto ;
                            }else {
                                photoModel.type = PFPhotoModelMediaTypePhoto;
                            }
                        }else {
                            photoModel.type = PFPhotoModelMediaTypePhoto;
                        }
                    }
                    [photoArray addObject:photoModel];
                }else if (asset.mediaType == PHAssetMediaTypeVideo){
                    photoModel.subType = PFPhotoModelMediaSubTypeVideo;
                    photoModel.type = PFPhotoModelMediaTypeVideo;
                    [videoArray addObject:photoModel];
                }
                photoModel.currentAlbumIndex = albumModel.index;
                [allArray addObject:photoModel];
                [previewArray addObject:photoModel];
                
                if(self.configuration.showDateSectionHeader){
                    NSDate * photoDate = photoModel.creationDate;
                    if(!currentIndexDate){
                        dateModel = [[PFPhotoDateModel alloc] init];
                        dateModel.date = photoDate;
                        sameDayArray = [[NSMutableArray alloc] init];
                        [sameDayArray addObject:photoModel];
                        photoModel.dateItem = sameDayArray.count - 1;
                        photoModel.dateSection = dateArray.count - 1;
                    }else{
                        if([self isSameDay:photoDate date2:currentIndexDate]){
                            [sameDayArray addObject:photoModel];
                            photoModel.dateItem = sameDayArray.count - 1;;
                            photoModel.dateSection = dateArray.count - 1;
                        }else{
                            dateModel.photoModelArray = sameDayArray;
                            sameDayArray = [[NSMutableArray alloc] init];
                            dateModel.date = photoDate;
                            [sameDayArray addObject:photoModel];
                            [dateArray addObject:dateModel];
                            photoModel.dateItem = sameDayArray.count - 1;
                            photoModel.dateSection = dateArray.count - 1;
                        }
                    }
                    if (firstSelectModel && !already) {
                        firstSelectModel.dateSection = dateArray.count - 1;
                        firstSelectModel.dateItem = sameDayArray.count - 1;
                        already = YES;
                    }
                    if (index == albumModel.result.count - 1) {
                        dateModel.photoModelArray = sameDayArray;
                    }
//                    if (!dateModel.location && self.configuration.sectionHeaderShowPhotoLocation) {
//                        if (photoModel.asset.location) {
//                            dateModel.location = photoModel.asset.location;
//                        }
//                    }
                    currentIndexDate = photoDate;
                }
                else {
                    photoModel.dateItem = allArray.count - 1;
                    photoModel.dateSection = 0;
                }
                index++;
            }
        }
        
    }
    
   
}


/**
 *  是否为同一天
 */
- (BOOL)isSameDay:(NSDate*)date1 date2:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

#pragma mark ----------------  懒加载

- (PFPhotoConfiguration *)configuration{
    if(!_configuration){
        _configuration = [[PFPhotoConfiguration alloc] init];
    }
    return _configuration;
}


@end

