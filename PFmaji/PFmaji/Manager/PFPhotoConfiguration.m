//
//  PFPhotoConfiguration.m
//  PFmaji
//
//  Created by perfayMini on 2018/12/6.
//  Copyright © 2018 huawei. All rights reserved.
//

#import "PFPhotoConfiguration.h"

@implementation PFPhotoConfiguration

- (instancetype)init{
    if(self = [super init]){
        [self setup];
    }
    return self;
}

- (void)setup{
    self.creationDateSort = YES;
    self.reverseDate = NO;
}


- (void)setClarityScale:(CGFloat)clarityScale {
    if (clarityScale <= 0.f) {
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            _clarityScale = 0.8;
        }else if ([UIScreen mainScreen].bounds.size.width == 375) {
            _clarityScale = 1.4;
        }else {
            _clarityScale = 1.7;
        }
    }else {
        _clarityScale = clarityScale;
    }
}

@end
