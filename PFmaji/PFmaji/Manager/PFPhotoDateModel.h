//
//  PFPhotoDateModel.h
//  PFmaji
//
//  Created by perfayMini on 2018/12/6.
//  Copyright © 2018 huawei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PFPhotoDateModel : NSObject
/**  位置信息 - 如果当前天数内包含带有位置信息的资源则有值 */
@property (strong, nonatomic) CLLocation *location;
/**  日期信息 */
@property (strong, nonatomic) NSDate *date;
/**  日期信息字符串 */
@property (copy, nonatomic) NSString *dateString;
/**  位置信息字符串 */
@property (copy, nonatomic) NSString *locationString;;
/**  同一天的资源数组 */
@property (copy, nonatomic) NSArray *photoModelArray;
/**  位置信息子标题 */
@property (copy, nonatomic) NSString *locationSubTitle;
/**  位置信息标题 */
@property (copy, nonatomic) NSString *locationTitle;

@property (strong, nonatomic) NSMutableArray *locationList;
@property (assign, nonatomic) BOOL hasLocationTitles;
@end

NS_ASSUME_NONNULL_END
