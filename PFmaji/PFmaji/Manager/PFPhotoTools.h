//
//  PFPhotoTools.h
//  PFmaji
//
//  Created by perfayMini on 2018/12/5.
//  Copyright © 2018 huawei. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PFAlbumModel;

NS_ASSUME_NONNULL_BEGIN

@interface PFPhotoTools : NSObject

+ (PHImageRequestID)getImageWithAlbumModel:(PFAlbumModel *)model size:(CGSize )size completion:(void (^)(UIImage *image, PFAlbumModel *model))completion;

+ (PHImageRequestID)getHighQualityFormatPhotoForPHAsset:(PHAsset *)asset size:(CGSize)size completion:(void(^)(UIImage *image,NSDictionary *info))completion error:(void(^)(NSDictionary *info))error;

@end

NS_ASSUME_NONNULL_END
