//
//  PFAlbumModel.h
//  PFmaji
//
//  Created by perfayMini on 2018/12/5.
//  Copyright © 2018 huawei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PFAlbumModel : NSObject
/**
 相册名称
 */
@property (copy, nonatomic) NSString *albumName;
@property (assign, nonatomic) NSInteger count;
/**
 封面Asset
 */
@property (strong, nonatomic) PHAsset *asset;

@property (strong, nonatomic) PHFetchResult *result;

@property (assign, nonatomic) NSInteger index;

@property (assign, nonatomic) NSInteger selectedCount;

@property (assign, nonatomic) NSUInteger cameraCount;

@property (strong, nonatomic) UIImage *tempImage;
@end

NS_ASSUME_NONNULL_END
