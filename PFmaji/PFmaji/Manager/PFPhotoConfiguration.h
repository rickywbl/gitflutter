//
//  PFPhotoConfiguration.h
//  PFmaji
//
//  Created by perfayMini on 2018/12/6.
//  Copyright © 2018 huawei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PFPhotoConfiguration : NSObject

/**
 照片列表是否按照片日期排序  默认YES
 */
@property (assign, nonatomic) BOOL creationDateSort;



/**
 小图照片清晰度 越大越清晰、越消耗性能
 设置太大的话获取图片资源时耗时长且内存消耗大可能会引起界面卡顿
 default：[UIScreen mainScreen].bounds.size.width
 320    ->  0.8
 375    ->  1.4
 other  ->  1.7
 */
@property (assign, nonatomic) CGFloat clarityScale;


/**
 照片列表按日期倒序 默认 NO
 */
@property (assign, nonatomic) BOOL reverseDate;

/**
 是否为单选模式 默认 NO
 会自动过滤掉gif、livephoto
 */
@property (assign, nonatomic) BOOL singleSelected;


/**
 是否需要显示日期section  默认YES
 */
@property (assign, nonatomic) BOOL showDateSectionHeader;


@end

NS_ASSUME_NONNULL_END
