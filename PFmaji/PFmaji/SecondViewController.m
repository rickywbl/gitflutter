//
//  SecondViewController.m
//  PFmaji
//
//  Created by perfayMini on 2018/12/5.
//  Copyright © 2018 huawei. All rights reserved.
//

#import "SecondViewController.h"
#import "PhtotCollectionViewCell.h"

@interface SecondViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>

@property(strong,nonatomic)UICollectionView * collectionView;
@property(strong,nonatomic)PFPhotoManager * phtotManager;
@property(strong,nonatomic)UICollectionViewFlowLayout * layout;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    self.layout.itemSize = CGSizeMake(self.view.frame.size.width / 4, self.view.frame.size.width / 4);
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:self.layout];
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"PhtotCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    [self.view addSubview:self.collectionView]; 
}


#pragma mark  ----  UICollectionViewDataSource,UICollectionViewDelegate

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PhtotCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor qmui_randomColor];
    PHAsset *  asset =  self.album.result[indexPath.row];
    [PFPhotoTools getHighQualityFormatPhotoForPHAsset:asset size:CGSizeMake(500, 500) completion:^(UIImage * _Nonnull image, NSDictionary * _Nonnull info) {
        cell.photo.image = image;
    } error:^(NSDictionary * _Nonnull info) {
        
    }];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.album.result.count;
;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

}



- (PFPhotoManager *)phtotManager{
    if(!_phtotManager){
        _phtotManager = [[PFPhotoManager alloc] init];
    }
    return _phtotManager;
}

- (UICollectionViewFlowLayout *)layout{
    if(!_layout){
        _layout = [[UICollectionViewFlowLayout alloc] init];
        _layout.minimumInteritemSpacing = 0;
        _layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    return _layout;
}
@end
