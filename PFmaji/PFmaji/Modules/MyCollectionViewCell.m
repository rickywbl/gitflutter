//
//  MyCollectionViewCell.m
//  PFmaji
//
//  Created by perfayMini on 2018/12/5.
//  Copyright © 2018 huawei. All rights reserved.
//

#import "MyCollectionViewCell.h"

@implementation MyCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-  (void)setModel:(PFAlbumModel *)model{
    _model = model;
    self.albumName.text =  model.albumName;
    _albumImageView.clipsToBounds = YES;
    _albumImageView.contentMode = UIViewContentModeScaleAspectFill;
    __weak typeof(self) weakSelf = self;
    [PFPhotoTools getImageWithAlbumModel:model size:CGSizeMake(self.frame.size.height * 1.6 , self.frame.size.height * 1.6) completion:^(UIImage * _Nonnull image, PFAlbumModel * _Nonnull model) {
        weakSelf.albumImageView.image = image;
    }];
}


@end
