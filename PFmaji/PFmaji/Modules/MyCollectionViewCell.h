//
//  MyCollectionViewCell.h
//  PFmaji
//
//  Created by perfayMini on 2018/12/5.
//  Copyright © 2018 huawei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *albumImageView;
@property (weak, nonatomic) IBOutlet UILabel *albumName;
@property(nonatomic,strong)PFAlbumModel * model;
@end

NS_ASSUME_NONNULL_END
