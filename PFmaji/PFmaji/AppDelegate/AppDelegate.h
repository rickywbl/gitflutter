//
//  AppDelegate.h
//  PFmaji
//
//  Created by perfayMini on 2018/12/4.
//  Copyright © 2018 huawei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

