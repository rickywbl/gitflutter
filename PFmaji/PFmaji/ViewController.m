//
//  ViewController.m
//  PFmaji
//
//  Created by perfayMini on 2018/12/4.
//  Copyright © 2018 huawei. All rights reserved.
//

#import "ViewController.h"
#import "MyCollectionViewCell.h"
#import "SecondViewController.h"

@interface ViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property(strong,nonatomic)UICollectionView * collectionView;
@property(strong,nonatomic)PFPhotoManager * phtotManager;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    XRWaterfallLayout * fallLayout = [[XRWaterfallLayout alloc] initWithColumnCount:2];
    [fallLayout setColumnSpacing:12 rowSpacing:12 sectionInset:(UIEdgeInsetsMake(12, 12, 12, 12))];
    fallLayout.itemHeightBlock = ^CGFloat(CGFloat itemHeight, NSIndexPath *indexPath) {
       
        return itemHeight * 1.2;
    };
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:fallLayout];
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"MyCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    [self.view addSubview:self.collectionView];
}



#pragma mark  ----  UICollectionViewDataSource,UICollectionViewDelegate

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MyCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    PFAlbumModel * model = self.phtotManager.allAlbums[indexPath.row];
    cell.model = model;
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.phtotManager.allAlbums.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PFAlbumModel * model = self.phtotManager.allAlbums[indexPath.row];
    SecondViewController * vc = [[SecondViewController alloc] init];
    vc.title = model.albumName;
    vc.album = model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (PFPhotoManager *)phtotManager{
    if(!_phtotManager){
        _phtotManager = [[PFPhotoManager alloc] init];
    }
    return _phtotManager;
}

@end
