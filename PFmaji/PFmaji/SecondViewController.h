//
//  SecondViewController.h
//  PFmaji
//
//  Created by perfayMini on 2018/12/5.
//  Copyright © 2018 huawei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SecondViewController : UIViewController
@property(nonatomic,strong)PFAlbumModel * album;
@end

NS_ASSUME_NONNULL_END
